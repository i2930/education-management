// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import {getAllWeeks, setNewWeek} from "@/prisma/weeks";

type Data = {
  name: string
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const requestMethod = req.method;
  let body;
  if(req.body) {
    body = JSON.parse(req.body);
  }
  else {
    body = {};
  }

  switch (requestMethod) {
    case 'GET':
      res.status(200).json(await getAllWeeks());
      break;
    case 'POST':
      res.status(200).json(await setNewWeek(body));
      break;
    default:
      res.status(200).json({ name: 'John Doe' })
  }
}
