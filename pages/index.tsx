import Image from 'next/image'
import { Inter } from 'next/font/google'
import Head from "next/head";
import Layout from "@/components/layout";
import Weeks from "@/components/weeks";



const inter = Inter({ subsets: ['latin'] })

export async function getServerSideProps(context) {
    const res = await fetch(`http://localhost:3000/api/weeks`);
    const data = await res.json();
    console.log(data);
    return {
        props: {
            data
        }
    }
}

export default function Home({data}) {
  return (
      <>
        <Head>
          <title>Productivity toll home page</title>
        </Head>
        <Layout>
            <Weeks weeks={data} ></Weeks>
        </Layout>
      </>
  )
}
