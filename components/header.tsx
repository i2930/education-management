export default function Header() {
    return (
        <div className="flex w-full h-20 bg-blue-400 items-center justify-between px-5" >
            <span>Productivity App</span>
            <div className="flex justify-center content-center w-10 h-10 rounded-full bg-cyan-200"></div>
        </div>
    );
}
