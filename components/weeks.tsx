import WeekItem from "@/components/week";

export default function Weeks({weeks}) {
    return <div className="flex p-20 px-0 justify-between flex-wrap" >
        {
            weeks.map((week) => (
                <WeekItem week={week} ></WeekItem>
            ))
        }
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
        <WeekItem></WeekItem>
    </div>
}
