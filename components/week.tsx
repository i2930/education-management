import styles from "./week-item.module.scss"
export default function WeekItem({week}) {
    return <div className={styles.weekItem + " flex w-full max-w-md h-80 flex-shrink justify-center items-center m-4 hover:scale-110 cursor-pointer duration-150 rounded-lg shadow-lg"}>
        {week.name}
    </div>
}
