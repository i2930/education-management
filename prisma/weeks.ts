import prisma from './prisma'

// READ
export const getAllWeeks = async () => {
  return await prisma.weeks.findMany({});
}

export const setNewWeek = async (weekData) => {
    return  await prisma.weeks.create({data: weekData});
  }
